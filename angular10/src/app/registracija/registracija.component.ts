import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import {SharedService} from 'src/app/shared.service';
import{Router} from '@angular/router';

@Component({
  selector: 'app-registracija',
  templateUrl: './registracija.component.html',
  styleUrls: ['./registracija.component.css']
})
export class RegistracijaComponent implements OnInit {

  constructor(private service:SharedService, private router:Router) { }

  @Input() reg:any;
  vardas:string;
  pavarde:string;
  slaptazodis:string;
  elektroninis_pastas:string;
  telefono_numeris:string;
  saskaitos_numeris:string;

  kliento_id:string;
  paskyros_sukurimo_laikas:string;

  @Output() newItemEvent = new EventEmitter<string>();

  ngOnInit(): void {
    this.vardas=this.reg.vardas;
    this.pavarde=this.reg.pavarde;
    this.slaptazodis=this.reg.slaptazodis;
    this.elektroninis_pastas=this.reg.elektroninis_pastas;
    this.telefono_numeris=this.reg.telefono_numeris;
    this.saskaitos_numeris=this.reg.saskaitos_numeris;
  }
  changeBool(){
    this.newItemEvent.emit("hi")
}

  addKlientas(){
    var val = {
      vardas:this.vardas,
      pavarde:this.pavarde,
      slaptazodis:this.slaptazodis,
      elektroninis_pastas:this.elektroninis_pastas,
      telefono_numeris:this.telefono_numeris,
      saskaitos_numeris:this.saskaitos_numeris,

    };
    this.service.addKlientas(val).subscribe(res=>{
      alert("Signup Succesfull");
      this.changeBool()
      this.router.navigate(['prisijungimas']);
    },err=>{
      alert("Something went wrong")
    }
    );
  }
/*
  updateDepartment(){
    var val = {DepartmentId:this.DepartmentId,
      DepartmentName:this.DepartmentName};
    this.service.updateDepartment(val).subscribe(res=>{
    alert(res.toString());
    });
  }*/

}
