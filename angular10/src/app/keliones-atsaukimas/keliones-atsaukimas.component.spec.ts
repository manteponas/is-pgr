import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KelionesAtsaukimasComponent } from './keliones-atsaukimas.component';

describe('KelionesAtsaukimasComponent', () => {
  let component: KelionesAtsaukimasComponent;
  let fixture: ComponentFixture<KelionesAtsaukimasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KelionesAtsaukimasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KelionesAtsaukimasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
