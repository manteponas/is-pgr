import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';
import{Router} from '@angular/router';
import { FormsModule } from '@angular/forms';
import {UserService} from 'src/app/shared.service';

@Component({
  selector: 'app-prisijungimas',
  templateUrl: './prisijungimas.component.html',
  styleUrls: ['./prisijungimas.component.css']
})
export class PrisijungimasComponent implements OnInit {
  slaptazodis:string;
  elektroninis_pastas:string;
  register=false;
  constructor(private service:SharedService, private router:Router,private userService: UserService) {}

  @Input() user
  @Output() newItemEvent = new EventEmitter<string>();
  ngOnInit(): void {
  }
  changeUser(value:any){
      this.newItemEvent.emit(value)
  }

  Login(){

    this.service.getKlientai().subscribe(res=>{
      const klientas = res.find((a:any)=>{return a.elektroninis_pastas === this.elektroninis_pastas && a.slaptazodis === this.slaptazodis})
      if(klientas){
        alert("Login success")
        this.userService.userChange(klientas)
        this.changeUser(klientas)
        this.router.navigate(['pagrindinis']);
      }
      else{
        alert("User not found")
      }
    },err=>{
      alert("Something went wrong")
    }
    );
  }
  changeBool(val:string){
    this.register=false;
  }

}
