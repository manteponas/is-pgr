import { Component, OnInit, Input } from '@angular/core';
import{Router} from '@angular/router';
import {UserService} from 'src/app/shared.service';

@Component({
  selector: 'app-pagrindinis',
  templateUrl: './pagrindinis.component.html',
  styleUrls: ['./pagrindinis.component.css']
})
export class PagrindinisComponent implements OnInit {
  
  constructor(private router:Router,private userService: UserService) { this.user=this.userService.user}
  user:any
  ngOnInit(): void {this.userService.change.subscribe(val =>{this.user = val})
  }

}
