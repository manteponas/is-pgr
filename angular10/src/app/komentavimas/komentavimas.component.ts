import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { FormsModule } from '@angular/forms';

@Component({
  selector: 'app-komentavimas',
  templateUrl: './komentavimas.component.html',
  styleUrls: ['./komentavimas.component.css']
})
export class KomentavimasComponent implements OnInit {
  komentaras:string
  senasKomentaras:string
  l=0
  flag=false
  constructor() { }
  @Output() newItemEvent = new EventEmitter<string>();
  changeKomentaras(value:any){
    this.newItemEvent.emit(value)
  } 

  ngOnInit(): void {
  }

  onChange(event:any){
    console.log(event)
    console.log(event.key.length)


    if(this.komentaras && event.key.length==1 && this.komentaras.length>200){
      alert("Maksimalus komentaro ilgis 200")
      event.preventDefault();
    }
  }


  check(event:any){
    console.log(this.komentaras)
    if(this.komentaras && this.komentaras.length>201){
      alert("Maksimalus komentaro ilgis 200")
      this.komentaras=this.senasKomentaras
    }
    else{
      this.senasKomentaras = this.komentaras
      this.changeKomentaras(this.komentaras)
    }
  }
  delete(){
    this.komentaras=""
    this.changeKomentaras("")
  }

}
