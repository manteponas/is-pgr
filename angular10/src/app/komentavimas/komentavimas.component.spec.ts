import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KomentavimasComponent } from './komentavimas.component';

describe('KomentavimasComponent', () => {
  let component: KomentavimasComponent;
  let fixture: ComponentFixture<KomentavimasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KomentavimasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KomentavimasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
