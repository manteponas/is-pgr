import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtaskaitosLaikotarpioIrKelionesPradziosPasirinkimasComponent } from './ataskaitos-laikotarpio-ir-keliones-pradzios-pasirinkimas.component';

describe('AtaskaitosLaikotarpioIrKelionesPradziosPasirinkimasComponent', () => {
  let component: AtaskaitosLaikotarpioIrKelionesPradziosPasirinkimasComponent;
  let fixture: ComponentFixture<AtaskaitosLaikotarpioIrKelionesPradziosPasirinkimasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtaskaitosLaikotarpioIrKelionesPradziosPasirinkimasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtaskaitosLaikotarpioIrKelionesPradziosPasirinkimasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
