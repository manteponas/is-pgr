import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MarsrutoPasirinkimasComponent } from './marsruto-pasirinkimas.component';

describe('MarsrutoPasirinkimasComponent', () => {
  let component: MarsrutoPasirinkimasComponent;
  let fixture: ComponentFixture<MarsrutoPasirinkimasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MarsrutoPasirinkimasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MarsrutoPasirinkimasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
