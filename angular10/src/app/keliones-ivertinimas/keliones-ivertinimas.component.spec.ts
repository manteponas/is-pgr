import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KelionesIvertinimasComponent } from './keliones-ivertinimas.component';

describe('KelionesIvertinimasComponent', () => {
  let component: KelionesIvertinimasComponent;
  let fixture: ComponentFixture<KelionesIvertinimasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KelionesIvertinimasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KelionesIvertinimasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
