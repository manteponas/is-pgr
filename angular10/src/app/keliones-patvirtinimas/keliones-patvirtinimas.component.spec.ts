import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KelionesPatvirtinimasComponent } from './keliones-patvirtinimas.component';

describe('KelionesPatvirtinimasComponent', () => {
  let component: KelionesPatvirtinimasComponent;
  let fixture: ComponentFixture<KelionesPatvirtinimasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KelionesPatvirtinimasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KelionesPatvirtinimasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
