import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VairuotojoLokacijaComponent } from './vairuotojo-lokacija.component';

describe('VairuotojoLokacijaComponent', () => {
  let component: VairuotojoLokacijaComponent;
  let fixture: ComponentFixture<VairuotojoLokacijaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VairuotojoLokacijaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VairuotojoLokacijaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
