import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NuomojamasAutomobilisComponent } from './nuomojamas-automobilis.component';

describe('NuomojamasAutomobilisComponent', () => {
  let component: NuomojamasAutomobilisComponent;
  let fixture: ComponentFixture<NuomojamasAutomobilisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NuomojamasAutomobilisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NuomojamasAutomobilisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
