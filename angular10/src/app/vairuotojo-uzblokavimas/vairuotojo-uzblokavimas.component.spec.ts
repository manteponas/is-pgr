import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VairuotojoUzblokavimasComponent } from './vairuotojo-uzblokavimas.component';

describe('VairuotojoUzblokavimasComponent', () => {
  let component: VairuotojoUzblokavimasComponent;
  let fixture: ComponentFixture<VairuotojoUzblokavimasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VairuotojoUzblokavimasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VairuotojoUzblokavimasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
