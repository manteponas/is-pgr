import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RezervacijosComponent } from './rezervacijos.component';

describe('RezervacijosComponent', () => {
  let component: RezervacijosComponent;
  let fixture: ComponentFixture<RezervacijosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RezervacijosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RezervacijosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
