import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BusenaComponent } from './busena.component';

describe('BusenaComponent', () => {
  let component: BusenaComponent;
  let fixture: ComponentFixture<BusenaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BusenaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BusenaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
