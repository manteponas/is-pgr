import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DepartmentComponent } from './department/department.component';
import { ShowDepComponent } from './department/show-dep/show-dep.component';
import { AddEditDepComponent } from './department/add-edit-dep/add-edit-dep.component';
import { EmployeeComponent } from './employee/employee.component';
import { ShowEmpComponent } from './employee/show-emp/show-emp.component';
import { AddEditEmpComponent } from './employee/add-edit-emp/add-edit-emp.component';
import{SharedService} from './shared.service';

import {HttpClientModule} from '@angular/common/http';
import {FormsModule,ReactiveFormsModule} from '@angular/forms';
import { RegistracijaComponent } from './registracija/registracija.component';
import { PagrindinisComponent } from './pagrindinis/pagrindinis.component';
import { KelioneComponent } from './kelione/kelione.component';
import { BusenaComponent } from './busena/busena.component';
import { AtsisakymasComponent } from './atsisakymas/atsisakymas.component';
import { KelionesPatvirtinimasComponent } from './keliones-patvirtinimas/keliones-patvirtinimas.component';
import { KelionesAtsaukimasComponent } from './keliones-atsaukimas/keliones-atsaukimas.component';
import { VairuotojoLokacijaComponent } from './vairuotojo-lokacija/vairuotojo-lokacija.component';
import { BlokavimasComponent } from './blokavimas/blokavimas.component';
import { ProfilisComponent } from './profilis/profilis.component';
import { PrisijungimasComponent } from './prisijungimas/prisijungimas.component';
import { KelionesUzsakymasComponent } from './keliones-uzsakymas/keliones-uzsakymas.component';
import { NuomojamasAutomobilisComponent } from './nuomojamas-automobilis/nuomojamas-automobilis.component';
import { AtaskaitosLaikotarpioIrKelionesPradziosPasirinkimasComponent } from './ataskaitos-laikotarpio-ir-keliones-pradzios-pasirinkimas/ataskaitos-laikotarpio-ir-keliones-pradzios-pasirinkimas.component';
import { KelionesPabaigaComponent } from './keliones-pabaiga/keliones-pabaiga.component';
import { VairuotojoAplikacijaComponent } from './vairuotojo-aplikacija/vairuotojo-aplikacija.component';
import { AtaskaitaComponent } from './ataskaita/ataskaita.component';
import { KelionesIvertinimasComponent } from './keliones-ivertinimas/keliones-ivertinimas.component';
import { MarsrutoPasirinkimasComponent } from './marsruto-pasirinkimas/marsruto-pasirinkimas.component';
import { NusiskundimaiComponent } from './nusiskundimai/nusiskundimai.component';
import { VairuotojaiComponent } from './vairuotojai/vairuotojai.component';
import { ReklaminiuZinuciuSukurimasComponent } from './reklaminiu-zinuciu-sukurimas/reklaminiu-zinuciu-sukurimas.component';
import { RezervacijosComponent } from './rezervacijos/rezervacijos.component';
import { AplikantosComponent } from './aplikantos/aplikantos.component';
import { ApdorotoNusiskundimoInformacijaComponent } from './apdoroto-nusiskundimo-informacija/apdoroto-nusiskundimo-informacija.component';
import { PatvirtinimasComponent } from './patvirtinimas/patvirtinimas.component';
import { AplikantoPatvirinimasComponent } from './aplikanto-patvirinimas/aplikanto-patvirinimas.component';
import { VairuotojoUzblokavimasComponent } from './vairuotojo-uzblokavimas/vairuotojo-uzblokavimas.component';
import { VairuotojuPasirinkimasComponent } from './vairuotoju-pasirinkimas/vairuotoju-pasirinkimas.component';
import { KomentavimasComponent } from './komentavimas/komentavimas.component';

@NgModule({
  declarations: [
    AppComponent,
    DepartmentComponent,
    ShowDepComponent,
    AddEditDepComponent,
    EmployeeComponent,
    ShowEmpComponent,
    AddEditEmpComponent,
    RegistracijaComponent,
    PagrindinisComponent,
    KelioneComponent,
    BusenaComponent,
    AtsisakymasComponent,
    KelionesPatvirtinimasComponent,
    KelionesAtsaukimasComponent,
    VairuotojoLokacijaComponent,
    BlokavimasComponent,
    ProfilisComponent,
    PrisijungimasComponent,
    KelionesUzsakymasComponent,
    NuomojamasAutomobilisComponent,
    AtaskaitosLaikotarpioIrKelionesPradziosPasirinkimasComponent,
    KelionesPabaigaComponent,
    VairuotojoAplikacijaComponent,
    AtaskaitaComponent,
    KelionesIvertinimasComponent,
    MarsrutoPasirinkimasComponent,
    NusiskundimaiComponent,
    VairuotojaiComponent,
    ReklaminiuZinuciuSukurimasComponent,
    RezervacijosComponent,
    AplikantosComponent,
    ApdorotoNusiskundimoInformacijaComponent,
    PatvirtinimasComponent,
    AplikantoPatvirinimasComponent,
    VairuotojoUzblokavimasComponent,
    VairuotojuPasirinkimasComponent,
    KomentavimasComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
