import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtsisakymasComponent } from './atsisakymas.component';

describe('AtsisakymasComponent', () => {
  let component: AtsisakymasComponent;
  let fixture: ComponentFixture<AtsisakymasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtsisakymasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtsisakymasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
