import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AplikantoPatvirinimasComponent } from './aplikanto-patvirinimas.component';

describe('AplikantoPatvirinimasComponent', () => {
  let component: AplikantoPatvirinimasComponent;
  let fixture: ComponentFixture<AplikantoPatvirinimasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AplikantoPatvirinimasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AplikantoPatvirinimasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
