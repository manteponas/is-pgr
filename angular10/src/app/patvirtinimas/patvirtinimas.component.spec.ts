import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PatvirtinimasComponent } from './patvirtinimas.component';

describe('PatvirtinimasComponent', () => {
  let component: PatvirtinimasComponent;
  let fixture: ComponentFixture<PatvirtinimasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PatvirtinimasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PatvirtinimasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
