import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VairuotojaiComponent } from './vairuotojai.component';

describe('VairuotojaiComponent', () => {
  let component: VairuotojaiComponent;
  let fixture: ComponentFixture<VairuotojaiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VairuotojaiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VairuotojaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
