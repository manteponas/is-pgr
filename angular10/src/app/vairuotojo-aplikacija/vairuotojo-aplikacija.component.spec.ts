import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VairuotojoAplikacijaComponent } from './vairuotojo-aplikacija.component';

describe('VairuotojoAplikacijaComponent', () => {
  let component: VairuotojoAplikacijaComponent;
  let fixture: ComponentFixture<VairuotojoAplikacijaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VairuotojoAplikacijaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VairuotojoAplikacijaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
