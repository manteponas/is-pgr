import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AtaskaitaComponent } from './ataskaita.component';

describe('AtaskaitaComponent', () => {
  let component: AtaskaitaComponent;
  let fixture: ComponentFixture<AtaskaitaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AtaskaitaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AtaskaitaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
