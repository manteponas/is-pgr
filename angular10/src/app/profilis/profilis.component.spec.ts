import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfilisComponent } from './profilis.component';

describe('ProfilisComponent', () => {
  let component: ProfilisComponent;
  let fixture: ComponentFixture<ProfilisComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfilisComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfilisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
