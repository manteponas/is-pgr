import { Component, OnInit,Input } from '@angular/core';
import {UserService} from 'src/app/shared.service';
import { ElementRef, ViewChild } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-profilis',
  templateUrl: './profilis.component.html',
  styleUrls: ['./profilis.component.css']
})
export class ProfilisComponent implements OnInit {
  vardas:string
  slaptazodis:string;
  elektroninis_pastas:string;
  constructor(private userService: UserService,private service:SharedService) 
  {this.user=this.userService.user;this.vardas=this.user.vardas;this.slaptazodis=this.user.slaptazodis;this.elektroninis_pastas=this.user.elektroninis_pastas}
  user:any
  flag=true

  @ViewChild('userInfo') list: ElementRef;

  ngOnInit(): void {
  }

  enableEdit(){
    this.flag=false
  }
  edit(){
    this.user.vardas = this.vardas
    this.user.slaptazodis = this.slaptazodis
    this.user.elektroninis_pastas = this.elektroninis_pastas
    this.userService.userChange(this.user)

    this.service.updateKlientas(this.user).subscribe(res=>{
      alert("Update Succesfull");
    },err=>{
      alert("Something went wrong")
    });



    this.flag=true
  }

}
