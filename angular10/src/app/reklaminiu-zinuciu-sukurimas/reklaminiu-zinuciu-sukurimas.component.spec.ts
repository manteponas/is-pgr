import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReklaminiuZinuciuSukurimasComponent } from './reklaminiu-zinuciu-sukurimas.component';

describe('ReklaminiuZinuciuSukurimasComponent', () => {
  let component: ReklaminiuZinuciuSukurimasComponent;
  let fixture: ComponentFixture<ReklaminiuZinuciuSukurimasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReklaminiuZinuciuSukurimasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReklaminiuZinuciuSukurimasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
