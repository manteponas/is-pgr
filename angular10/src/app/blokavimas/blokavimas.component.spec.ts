import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BlokavimasComponent } from './blokavimas.component';

describe('BlokavimasComponent', () => {
  let component: BlokavimasComponent;
  let fixture: ComponentFixture<BlokavimasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BlokavimasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BlokavimasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
