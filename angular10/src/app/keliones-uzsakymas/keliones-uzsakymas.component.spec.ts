import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KelionesUzsakymasComponent } from './keliones-uzsakymas.component';

describe('KelionesUzsakymasComponent', () => {
  let component: KelionesUzsakymasComponent;
  let fixture: ComponentFixture<KelionesUzsakymasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KelionesUzsakymasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KelionesUzsakymasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
