import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NusiskundimaiComponent } from './nusiskundimai.component';

describe('NusiskundimaiComponent', () => {
  let component: NusiskundimaiComponent;
  let fixture: ComponentFixture<NusiskundimaiComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NusiskundimaiComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NusiskundimaiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
