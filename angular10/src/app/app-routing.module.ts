import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import {EmployeeComponent} from "./employee/employee.component";

import {DepartmentComponent} from './department/department.component';
import {RegistracijaComponent} from './registracija/registracija.component';
import {PagrindinisComponent} from './pagrindinis/pagrindinis.component';

import {KelioneComponent} from './kelione/kelione.component';
import {BusenaComponent} from './busena/busena.component';
import {AtsisakymasComponent} from './atsisakymas/atsisakymas.component';
import {KelionesPatvirtinimasComponent} from './keliones-patvirtinimas/keliones-patvirtinimas.component';
import {KelionesAtsaukimasComponent} from './keliones-atsaukimas/keliones-atsaukimas.component';
import {VairuotojoLokacijaComponent} from './vairuotojo-lokacija/vairuotojo-lokacija.component';
import {BlokavimasComponent} from './blokavimas/blokavimas.component';
import {ProfilisComponent} from './profilis/profilis.component';
import {PrisijungimasComponent} from './prisijungimas/prisijungimas.component';
import {KelionesUzsakymasComponent} from './keliones-uzsakymas/keliones-uzsakymas.component';
import {NuomojamasAutomobilisComponent} from './nuomojamas-automobilis/nuomojamas-automobilis.component';
import {AtaskaitosLaikotarpioIrKelionesPradziosPasirinkimasComponent} from './ataskaitos-laikotarpio-ir-keliones-pradzios-pasirinkimas/ataskaitos-laikotarpio-ir-keliones-pradzios-pasirinkimas.component';
import {KelionesPabaigaComponent} from './keliones-pabaiga/keliones-pabaiga.component';
import {VairuotojuPasirinkimasComponent} from './vairuotoju-pasirinkimas/vairuotoju-pasirinkimas.component';
import {VairuotojoAplikacijaComponent} from './vairuotojo-aplikacija/vairuotojo-aplikacija.component';
import {AtaskaitaComponent} from './ataskaita/ataskaita.component';
import {KelionesIvertinimasComponent} from './keliones-ivertinimas/keliones-ivertinimas.component';
import {MarsrutoPasirinkimasComponent} from './marsruto-pasirinkimas/marsruto-pasirinkimas.component';
import {ReklaminiuZinuciuSukurimasComponent} from './reklaminiu-zinuciu-sukurimas/reklaminiu-zinuciu-sukurimas.component';
import {NusiskundimaiComponent} from './nusiskundimai/nusiskundimai.component';
import {VairuotojaiComponent} from './vairuotojai/vairuotojai.component';
import {RezervacijosComponent} from './rezervacijos/rezervacijos.component';
import {AplikantosComponent} from './aplikantos/aplikantos.component';
import {ApdorotoNusiskundimoInformacijaComponent} from './apdoroto-nusiskundimo-informacija/apdoroto-nusiskundimo-informacija.component';
import {PatvirtinimasComponent} from './patvirtinimas/patvirtinimas.component';
import {AplikantoPatvirinimasComponent} from './aplikanto-patvirinimas/aplikanto-patvirinimas.component';
import {VairuotojoUzblokavimasComponent} from './vairuotojo-uzblokavimas/vairuotojo-uzblokavimas.component';

const routes: Routes = [
{path:'employee',component:EmployeeComponent},
{path:'department',component:DepartmentComponent},
{path:'registracija',component:RegistracijaComponent},
{path:'pagrindinis',component:PagrindinisComponent},

{path: 'kelione',component:KelioneComponent},
{path: 'busena',component:BusenaComponent},
{path: 'atsisakymas',component:AtsisakymasComponent},
{path: 'keliones_patvirtinimas',component:KelionesPatvirtinimasComponent},
{path: 'keliones_atsaukimas',component:KelionesAtsaukimasComponent},
{path: 'vairuotojo_lokacija',component:VairuotojoLokacijaComponent},
{path: 'blokavimas',component:BlokavimasComponent},
{path: 'profilis',component:ProfilisComponent},
{path: 'prisijungimas',component:PrisijungimasComponent},
{path: 'keliones_uzsakymas',component:KelionesUzsakymasComponent},
{path: 'nuomuojamas_automobilis',component:NuomojamasAutomobilisComponent},
{path: 'ataskaitos_laikotarpio_ir_keliones_pradzios_pasirinkimas',component:AtaskaitosLaikotarpioIrKelionesPradziosPasirinkimasComponent},
{path: 'keliones_pabaiga',component:KelionesPabaigaComponent},
{path: 'vairuotoju_pasirinkimas',component:VairuotojuPasirinkimasComponent},
{path: 'vairuotojo_aplikacija',component:VairuotojoAplikacijaComponent},
{path: 'ataskaita',component:AtaskaitaComponent},
{path: 'keliones_ivertinimas',component:KelionesIvertinimasComponent},
{path: 'marsruto_pasirinkimas',component:MarsrutoPasirinkimasComponent},
{path: 'reklaminiu_zinučiu_sukurimas',component:ReklaminiuZinuciuSukurimasComponent},
{path: 'nusiskundimai',component:NusiskundimaiComponent},
{path: 'vairuotojai',component:VairuotojaiComponent},
{path: 'rezervacijos',component:RezervacijosComponent},
{path: 'aplikantos',component:AplikantosComponent},
{path: 'apdoroto_nusiskundimo_informacija',component:ApdorotoNusiskundimoInformacijaComponent},
{path: 'patvirtinimas',component:PatvirtinimasComponent},
{path: 'aplikanto_patvirinimas',component:AplikantoPatvirinimasComponent},
{path: 'vairuotojo_uzblokavimas',component:VairuotojoUzblokavimasComponent},

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
