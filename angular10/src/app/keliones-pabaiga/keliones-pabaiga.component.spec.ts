import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KelionesPabaigaComponent } from './keliones-pabaiga.component';

describe('KelionesPabaigaComponent', () => {
  let component: KelionesPabaigaComponent;
  let fixture: ComponentFixture<KelionesPabaigaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KelionesPabaigaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KelionesPabaigaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
