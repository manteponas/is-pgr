import { ComponentFixture, TestBed } from '@angular/core/testing';

import { KelioneComponent } from './kelione.component';

describe('KelioneComponent', () => {
  let component: KelioneComponent;
  let fixture: ComponentFixture<KelioneComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ KelioneComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(KelioneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
