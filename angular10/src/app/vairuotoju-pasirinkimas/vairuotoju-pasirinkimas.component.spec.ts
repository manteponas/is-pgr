import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VairuotojuPasirinkimasComponent } from './vairuotoju-pasirinkimas.component';

describe('VairuotojuPasirinkimasComponent', () => {
  let component: VairuotojuPasirinkimasComponent;
  let fixture: ComponentFixture<VairuotojuPasirinkimasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VairuotojuPasirinkimasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VairuotojuPasirinkimasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
