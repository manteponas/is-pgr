import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AplikantosComponent } from './aplikantos.component';

describe('AplikantosComponent', () => {
  let component: AplikantosComponent;
  let fixture: ComponentFixture<AplikantosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AplikantosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AplikantosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
