import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ApdorotoNusiskundimoInformacijaComponent } from './apdoroto-nusiskundimo-informacija.component';

describe('ApdorotoNusiskundimoInformacijaComponent', () => {
  let component: ApdorotoNusiskundimoInformacijaComponent;
  let fixture: ComponentFixture<ApdorotoNusiskundimoInformacijaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ApdorotoNusiskundimoInformacijaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ApdorotoNusiskundimoInformacijaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
