﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;
using System.Data;
using WebAPI.Models;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace WebAPI.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class KlientasController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _env;

        public KlientasController(IConfiguration configuration, IWebHostEnvironment env)
        {
            _configuration = configuration;
            _env = env;
        }

        [HttpGet]
        public JsonResult Get()
        {
            //TODO patikrint ar veikia data
            string query = @"
                    select id_Klientas, vardas, pavarde, slaptazodis, elektroninis_pastas, telefono_numeris, saskaitos_numeris,
                    convert(varchar(10),paskyros_sukurimo_laikas,120) as DateOfJoining,lygis,busena
                    from dbo.Klientas
                    ";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("EmployeeAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult(table);
        }


        [HttpPost]
        public JsonResult Post(Klientas klt)
        {
            string query = @"
                    insert into dbo.Klientas 
                    (vardas,pavarde,slaptazodis,elektroninis_pastas,telefono_numeris,saskaitos_numeris,paskyros_sukurimo_laikas,lygis,busena)
                    values 
                    (
                    '" + klt.vardas + @"'
                    ,'" + klt.pavarde + @"'
                    ,'" + klt.slaptazodis + @"'
                    ,'" + klt.elektroninis_pastas + @"'
                    ,'" + klt.telefono_numeris + @"'
                    ,'" + klt.saskaitos_numeris + @"'
                    ,'" + klt.paskyros_sukurimo_laikas + @"'
                    ,'" + klt.lygis + @"'
                    ,'" + "1" + @"'
                    )
                    ";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("EmployeeAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Added Successfully");
        }


        [HttpPut]
        public JsonResult Put(Klientas klt)
        {
            string query = @"
                    update dbo.Klientas set 
                    vardas = '" + klt.vardas + @"'
                    ,pavarde = '" + klt.pavarde + @"'
                    ,slaptazodis = '" + klt.slaptazodis + @"'
                    ,elektroninis_pastas = '" + klt.elektroninis_pastas + @"'
                    ,telefono_numeris = '" + klt.telefono_numeris + @"'
                    ,saskaitos_numeris = '" + klt.saskaitos_numeris + @"'
                    ,paskyros_sukurimo_laikas = '" + klt.paskyros_sukurimo_laikas + @"'
                    ,lygis = '" + klt.lygis + @"'
                    ,busena = '" + klt.busena + @"'
                    where id_Klientas = " + klt.id_Klientas + @" 
                    ";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("EmployeeAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Updated Successfully");
        }


        [HttpDelete("{id}")]
        public JsonResult Delete(int id)
        {
            string query = @"
                    delete from dbo.Klientas
                    where kliento_id = " + id + @" 
                    ";
            DataTable table = new DataTable();
            string sqlDataSource = _configuration.GetConnectionString("EmployeeAppCon");
            SqlDataReader myReader;
            using (SqlConnection myCon = new SqlConnection(sqlDataSource))
            {
                myCon.Open();
                using (SqlCommand myCommand = new SqlCommand(query, myCon))
                {
                    myReader = myCommand.ExecuteReader();
                    table.Load(myReader); ;

                    myReader.Close();
                    myCon.Close();
                }
            }

            return new JsonResult("Deleted Successfully");
        }
    }
}
