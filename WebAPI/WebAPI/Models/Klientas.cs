﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebAPI.Models
{
    public class Klientas
    {
        public int id_Klientas { get; set; }
        public string vardas { get; set; }
        public string pavarde { get; set; }
        public string slaptazodis { get; set; }
        public string elektroninis_pastas { get; set; }

        public string telefono_numeris { get; set; }

        public string saskaitos_numeris { get; set; }
        public string paskyros_sukurimo_laikas { get; set; }
        public string lygis { get; set; }
        public string busena { get; set; }

    }
}
